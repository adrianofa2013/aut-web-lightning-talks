#language:pt
@login
Funcionalidade: Remover filmer
        Para que eu possa manter o catálogo atualizado
        Sendo um gesto de catálogo que encontrou um título cancelado/que não tem uma boa aceitação pelo público
        Posso remover este item
    
    Cenario: Confirmar exclusão
        Dado que "dbz" está no catálogo
        Quando eu solicito a exclusão
        E eu confimo a solicitação
        Então este item deve ser removido da lista do catálogo

    @rm_movie
    Cenario: Cancelar da exclusão
        Dado que "10_coisas" está no catálogo
        Quando eu solicito a exclusão
        Mas cancelo a solicitação
        Então este item deve permanecer no catálogo