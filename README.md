# aut-web-lightning-talks

*Esse é um projeto com um curso de automação que estou realizando. Ele é um ótimo template para testes automatizados utilizando as tecnologias:* 

*  Cumbumber 
*  Capybaras
*  RSpec
*  Ruby

*Não é possível rodar ele com essa url pois estou rodando o projeto localmente, esse repositório serve para mostrar as funções e os padrões utilizado.*

Para clonar o projeto utilize no git bash:

`git clone https://gitlab.com/adrianofa2013/aut-web-lightning-talks.git`